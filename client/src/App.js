import React, { Component } from 'react';
import './App.css';
import { ReactComponent as Logo } from './beerlogo2.svg';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Cervejas from './components/Cervejas'
import CervejaDetalhe from './components/CervejaDetalhe';
import CervejasCTB from './components/TQuery/CervejasCTB';
import CervejasCTM from './components/TQuery/CervejasCTM';
import CervejasCTA from './components/TQuery/CervejasCTA';
import CervejasCAB from './components/AQuery/CervejasCAB';
import CervejasCAM from './components/AQuery/CervejasCAM';
import CervejasCAA from './components/AQuery/CervejasCAA';
import CervejasCAMA from './components/AQuery/CervejasCAMA';
import CervejaDetalheTest from './components/CervejaDetalheTest';

//Proxy pra localhost no package.json do client
const client = new ApolloClient({
  uri: '/graphql'});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
      <Router>
      <div className="container justify-content-center align-items-center">
      <hr></hr>
      <h3 className="text-center">AskMe
        <small className="text-warning">Brejas</small>
      </h3>
      <hr>
      </hr>
      <Logo width='100%' height='200px' display='block' 
         margin='auto'/>
       <Route exact path="/" component={Cervejas}/>
       <Route exact path="/ctb" component={CervejasCTB}/>
       <Route exact path="/ctm" component={CervejasCTM}/>
       <Route exact path="/cta" component={CervejasCTA}/>
       <Route exact path="/cab" component={CervejasCAB}/>
       <Route exact path="/cam" component={CervejasCAM}/>
       <Route exact path="/caa" component={CervejasCAA}/>
       <Route exact path="/cama" component={CervejasCAMA}/>
       <Route exact path="/cerveja/:id" component={CervejaDetalheTest}/>
      </div>
      </Router>
      </ApolloProvider>
    );
  }
}

export default App;

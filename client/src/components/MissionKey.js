import React from 'react';
import { Link } from 'react-router-dom';

export default function MissionKey() {
  return (
    <div className="col-md-12 text-center">
    <div className="btn-group my-3btn-group-toggle" data-toggle="buttons" role="group">
      <Link to="/ctb" className="btn btn-warning">Baixo</Link>
      <Link to="/ctm" className="btn btn-success">Médio</Link>
      <Link to="/cta" className="btn btn-danger">Alto</Link>
      <Link to="/" className="btn btn-secundary">Todas</Link>
    </div>
    </div>
    
  )
}

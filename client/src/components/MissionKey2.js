import React from 'react';
import { Link } from 'react-router-dom';

export default function MissionKey2() {
  return (
    <div className="col-md-12 text-center">
    <div className="btn-group my-3 my-3btn-group-toggle" data-toggle="buttons" role="group">
      <Link to="/cab" className="btn btn-warning">Baixo</Link>
      <Link to="/cam" className="btn btn-success">Médio</Link>
      <Link to="/caa" className="btn btn-primary">Amargo</Link>
      <Link to="/cama" className="btn btn-danger">Muito Amargo</Link>
      <Link to="/" className="btn btn-secundary">Todas</Link>
    </div>
    </div>
  )
}

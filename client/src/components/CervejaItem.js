import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

export default function CervejaItem({ cervejas : {id
    ,tagline, name, image_url, abv, ibu}

}) {
  return <div className="card border-light mb-3  text-white
  justify-content-center align-items-center"  >
     <div className="row h-100 justify-content-center align-items-center"> 
     <div className="col-md-9">
     <h4 className="text-center">{name}</h4>
     <h5 className="text-center">{tagline}</h5>
     <hr></hr>
     <img src={image_url} alt="xd" 
     className="rounded mx-auto d-block" height="200px">
     </img>
     <hr></hr>
     <h6 className="text-center">Teor Alcóolico : <span className={classNames({
         'text-warning': abv <= 5.0,
         'text-success': abv <= 6.0 && abv > 5.0,
         'text-danger': abv >6
     })}>{abv}%</span></h6>
     <h6 className="text-center">Amargor : <span className={classNames({
         'text-warning': ibu < 10.0,
         'text-success': ibu < 35.0 && ibu > 10.0,
         'text-primary': ibu < 60.0 && ibu > 35.0,
         'text-danger': ibu > 60.0
     })}>{ibu}</span></h6>
     </div>
     </div>

     <div className="col-md-4 justify-content-center align-items-center">
     <Link to={`/cerveja/${id}`} className="btn btn-outline-secondary mx-auto d-block">Detalhes</Link>
     </div>

     

    </div>;
  
}

import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import CervejaItem from './CervejaItem';
import MissionKey from './MissionKey';
import MissionKey2 from './MissionKey2';

const CERVEJAS_QUERY = gql`
query CervejasQuery{
    cervejas{
        id
        name
        tagline
        image_url
        abv
        ibu
      }
}
`;



export class Cervejas extends Component {
  render() {
    return (
      <Fragment>
        <hr></hr>
        <h5 className="display-10 my-10 text-center">Teor Alcóolico</h5>
        <MissionKey/>
        <p>.
        </p>
        <h5 className="display-10 my-10 text-center">Amargor</h5>
        <MissionKey2/>
        <Query query={CERVEJAS_QUERY}>
        {({ loading, error, data}) => {
                if (loading) return <h3>Carregando...</h3>;
                if (error) return console.log(error);


                return <Fragment>
                
                    {
                        data.cervejas.map(cervejas => (
                            <CervejaItem key={cervejas.id} cervejas={cervejas}/>
                        ))
                    }
                </Fragment>;
                 
                
            }
        }
        </Query>
      </Fragment>
    );
  }
}

export default Cervejas

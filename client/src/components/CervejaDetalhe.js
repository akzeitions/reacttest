import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

const CERVEJAID_QUERY = gql`
 query CervejaQuery($id: Int!){
    cerveja(id : $id){
        id
       name
       tagline
       image_url
       abv
       ibu
     }
}
`;


export class CervejaDetalhe extends Component {
  render() {
    let { id } = this.props.match.params;
    id = parseInt(id);
    return (
    <Fragment>
        <Query query={CERVEJAID_QUERY} variables={{ id }}>
        {({ loading, error, data  }) => {
                if (loading) return <h4>Carregando...</h4>;
                if (error) console.log(error);

                const {
                  id,
                  name,
                  tagline,
                  image_url,
                  abv,
                  ibu
              } = data.cerveja;
              


                console.log(data)
                    
                return (
                <div>
                    <h1 className="display-4 my-3">
                    Cerveja : {name} {id}
                    </h1>
                    <h4 className="mb-3">Detalhes da Cerveja</h4>
                    <ul className="list-group">
                    <li className="list-group-item">
                    Nome : {name}
                    </li>
                    <li className="list-group-item">
                    Descrição : {tagline}
                    </li>
                    <li className="list-group-item">
                    Teor Alcóolico : {abv}%
                    </li>
                    <li className="list-group-item">
                    Amargor : {ibu} {image_url} 
                    </li>
                    </ul>

                   
                    <hr/>
                    <Link to="/" className="btn btn-secondary">
                  Back
                </Link>
              </div>
            );
          }}
        </Query>
      </Fragment>
    );
  }
}

export default CervejaDetalhe;

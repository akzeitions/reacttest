const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema')
const cors = require('cors');
const path = require ('path');

//Setup simples do express-graphql
const app = express();

//Cross-origin :)
app.use(cors());

app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}));

//Redirecionando para a index quando atingirem qualquer rota
app.use(express.static('public'));
app.get('*', (req,res) =>{
  res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

//Configuração de variável de ambiente (Facilita Deploy) ou porta 5000
const PORT = process.env.PORT || 5000;

//Visualização no console.
app.listen(PORT, () => console.log(`Server iniciado na porta ${PORT}`));
#Teste de Frontend React [SOMENTE ALOCADO]

Olá, sou o Thiago e resolvi parar um pouquinho meu TCC (quase no final kkk) para fazer este teste.

Utilizei apenas 1 dia para fazê-lo, e ficaram faltando algumas partes do que considero como finalização do projeto.

Já utilizei React em um projeto que fiz para o Senai anteriormente (React/Java Web/Java Android/PostgreSQL), e estou aprendendo uma nova Stack agora (MongoDB/Express/React/Node [Famoso Mern]).

Neste projeto não cheguei a utilizar o MongoDB, pois não era necessário, mas utilizei algumas ferramentas interessantes que vocês podem avaliar no código (Apollo GraphQL para consumir os dados de forma seletiva na API, por exemplo)

Obs: Sei que a parte visual não ficou nem um pouco agradável, mas pensei em demonstrar mais a forma seletiva de lidar com a API.

Obs2: O que ficou faltando pra uma atualização posterior foi

1. Melhorar (bastante) a interface
2. Criar rotas e links nos botões pra selecionar os tipos de cervejas destacados
3. Criar e preencher mais campos no detalhamento de cada cerveja (algum erro no meu resolve está trazendo o array com variaveis indefinidas, preciso checar com calma)
4. Implementar campo de busca dinâmico

[{ linkApi: 'https://punkapi.com/documentation/v2' }]


##Regras
Utilizei vários componentes do bootstrap 4, e o tema utilizado foi o Cyborg (https://bootswatch.com/cyborg/).
Poderia ter pego uma landing page pronta (ficaria mais bonito), mas preferi fazer do zero.
Apollo-GraphQL e React-router-dom foram as principais ferramentas que utilizei dentro do React.

##Só isso?
Fiz deploy do projeto no Heroku pra facilitar:

https://intense-escarpment-62128.herokuapp.com/

##Obrigado pelo tempo utilizado avaliando o projeto :)!
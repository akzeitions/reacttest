const axios = require ('axios');

const {GraphQLObjectType, GraphQLSchema, GraphQLFloat,
     GraphQLList, GraphQLString, GraphQLInt, GraphQLNonNull} = require('graphql');


//Variáveis do objeto CervejaType
const CervejaType = new GraphQLObjectType({
    name: 'Cerveja',
    fields: () => ({
      id: {type : GraphQLInt},
      name: { type : GraphQLString},
      tagline: { type: GraphQLString},
      image_url: { type: GraphQLString},
      abv : { type: GraphQLFloat},
      ibu : { type: GraphQLFloat},

    })
  });

  const CervejaDetalheType = new GraphQLObjectType({
    name: 'CervejaDetalhe',
    fields: () => ({
      id: {type : GraphQLInt},
      name: { type : GraphQLString},
      tagline: { type: GraphQLString},
      image_url: { type: GraphQLString},
      abv : { type: GraphQLFloat},
      ibu : { type: GraphQLFloat},
      first_brewed : { type :GraphQLString},
      ebc : { type :GraphQLFloat},
      ph : { type :GraphQLFloat},
      attenuation_level : { type :GraphQLFloat}
    })
  });
  

  // Root Query com Axios
  const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields:{ 
        //Cerveja individual buscada por ID
     cerveja: {
            type: new GraphQLList(CervejaDetalheType),      
            args: {
                id : {
                  type : new GraphQLNonNull (GraphQLInt)
                }
            },
            resolve(parent, args) {
              return axios
                .get(`https://api.punkapi.com/v2/beers/${args.id}`)
                .then(res => res.data);
               
            }
          },
        //Lista de todas as cervejas
      cervejas: {
        type: new GraphQLList(CervejaType),
        resolve(parent, args) {
          return axios
            .get('https://api.punkapi.com/v2/beers')
            .then(res => res.data);
        }
      },
      cteorbaixo:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?abv_lt=5`)
          .then(res => res.data);
        }
      }, 
      cteormedio:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?abv_gt=5&abv_lt=6`)
          .then(res => res.data);
        }
      },
      cteoralto:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?abv_gt=7`)
          .then(res => res.data);
        }
      },
      camargorbaixo:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?ibu_lt=11`)
          .then(res => res.data);
        }
      },
      camargormedio:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?ibu_gt=10&ibu_lt=35`)
          .then(res => res.data);
        }
      },
      camargoralto:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?ibu_gt=35&ibu_lt=60`)
          .then(res => res.data);
        }
      },
      camargormuitoalto:{
        type: new GraphQLList(CervejaType),
        resolve(parent, args){
          return axios.get(`https://api.punkapi.com/v2/beers?ibu_gt=60`)
          .then(res => res.data);
        }
      },


  
    }
  });
  
  module.exports = new GraphQLSchema({
    query: RootQuery
  });